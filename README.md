
AISCO: Adaptive Information Systems for Charity Organizations

Based on the requirement, the mandatory features of AISCO are:

Program -- Program.abs

PublicationSystem -- Publication.abs

FinancialReport -- FinancialReport.abs

The optional features are ObjectiveTarget and DonationData.

The detail of features is modeled in feature model (COS_FeatureModel.abs)


Based on the variability,
there are 16 child features that are implemented on delta modules.

The delta module's name begins with letter D, 
for example DContinuous.abs is delta that implement feature continous.
The detail configuration between deltas and features is modeled in PL Configuration (COSPL.abs).

We defined the AISCO products in (COSProduct.abs),
there are 4 example products, SekolahBermainMatahari, PKPU, RamadhanForKids, and BeriBuku. 

----------------------------------------------------

Core product of AISCO should at least provide:

(a) information about activity of the organization:
    which include:
    name, PIC, Initial activity, contact information,
    Brief description. 

(b) publication system that helps the organization report their program execution.

(c) finance report can be uploaded or generated.
    It indicated the need of finance report, which the data
    can be taken from a specific uploaded documents
    or generated from the income and expense feature.

(d) the income should contain: accountcode, amount, donor,
    means (bank transfer, or cash, item), confirmation status,
    softcopy of the proof, type of donation (general or member)

(e) the expense should contain: accountcode, amount, Person in Charge
